$(function () { $('table.table-sort').tablesort(); });

$(document).ready(function() {
	$('.footer').html($('.table-sort-search-container'));
	$('.header').append('<table id="bot_head"></table>');
	$('#bot_head').html($('#bot_results thead'));

	$('.table-sort-search-input').after('<a href="#login_form" data-toggle="modal"  id="login_btn" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-user"></span></a>');
	$('.table-sort-search-input').after('<a href="#infos" data-toggle="modal"  id="info_btn" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-info-sign"></span></a>');
	
	$('#bot_head th:eq(0)').html('<a href="scan.php" id="bot_atom" style="top:0;z-index:998"><img src="images/rss.png" alt="RSS"/></a>');
	$('#bot_head th:eq(8) .table-sort-control').before('<a href="scan.php?atom=framakey" style="position:absolute;right:-15px;z-index:998"><img src="images/rss.png" alt="RSS"/></a>');
	$('#bot_head th:eq(9) .table-sort-control').before('<a href="scan.php?atom=framapack" style="position:absolute;right:-15px;z-index:998"><img src="images/rss.png" alt="RSS"/></a>');

	$.ajax({
		type: "GET",
		url: "js/keep.js",
		dataType: "script"
	});
});

<?php
require_once('config.php');

$search = isset($_GET["s"]) ? strtolower(preg_replace('/[^A-Za-z0-9-\-\+\_\!]/', '',$_GET["s"])) : '';

$bang = explode("!",$search);

// Gestion de la connexion/déconnexion
$admin=false;
if(!isset($_POST['logout'])){
	session_start();
	if(isset($_POST['user_id'])) { $_SESSION['login']=$_POST['user_id'];}
	if(isset($_POST['user_pwd'])) { $_SESSION['pass']=$_POST['user_pwd']; header('Location: index.php');} // Évite le message de confirmation du navigateur
	if(isset($_SESSION['login']) && isset($_SESSION['pass'])) {
		if(md5($_SESSION['login'].':'.$_SESSION['pass'])==$login_pass) {
			$admin=true;
		}
	}
} else {
	session_start();session_unset();session_destroy();
}

// Parcours du cache apps
$file = file('cache/framabot.txt');
$fb = json_decode($file[0], true);
if (filemtime('cache/framabot.txt')+$delay<time() || $admin) {

	$apps[0]=array(
		'_meta_pointer_' => $fb[0]['_meta_pointer_']
	);

	$cache = scandir('cache/apps');
	for($i=0;$i<count($cache);$i++) {
		if (substr($cache[$i],0,1)!="." && substr($cache[$i], -4, 4)=='.txt') {
			$file = file('cache/apps/'.$cache[$i]);
			$apps[substr($cache[$i], 0,-4)]= json_decode($file[0], true);
		}
	}

	// Mets les données en cache : framabot.txt
	$fb_txt = fopen('cache/framabot.txt', 'wb');
	fwrite($fb_txt, json_encode($apps)); fclose($fb_txt);
} else {
	$apps = $fb;
}

// Lecture du fichier Framadvd
$file = file('cache/framadvd.txt');
$fd = json_decode($file[0], true);

// Lecture du fichier Framadvd école
$file = file('cache/framadvde.txt');
$fe = json_decode($file[0], true);

// Lecture du fichier Framapack
$file = file('cache/framapack.txt');
$fp = json_decode($file[0], true);

// Lecture du fichier Framakey
$file = file('cache/framakey.txt');
$fk = json_decode($file[0], true);

// Lecture du fichier Ubuntu
$file = file('cache/ubuntu.txt');
$ub = json_decode($file[0], true);

// Lecture du fichier Framasoft
$file = file('cache/framasoft.txt');
$fst = json_decode($file[0], true);

/*************************
 * Traitement des données
 *************************/
$result = '';
// Parcours la liste des apps en cache
foreach ($apps as $app) {
	if(isset($app['name'])) {
		// Parcours la liste des apps fd
		$framadvd='';$fd_statut='';
		if(isset($fd[$app['name']])) {
			$fd_statut = compareVersion($app['version'], $fd[$app['name']]['version']);
			$framadvd = '
				<span class="hs">_fd</span>
				<img src="images/cv'.$fd_statut.'.png" alt="'.$fd_statut.'" /><span class="statut">'.$fd_statut.'</span> '.$fd[$app['name']]['version'].'
			';
		}

		// Parcours la liste des apps fe
		$framadvde='';$fe_statut='';
		if(isset($fe[$app['name']])) {
			$fe_statut = compareVersion($app['version'], $fe[$app['name']]['version']);
			$framadvde = '
				<span class="hs">_fe</span>
				<img src="images/cv'.$fe_statut.'.png" alt="'.$fe_statut.'" /><span class="statut">'.$fe_statut.'</span> '.$fe[$app['name']]['version'].'
			';
		}
		// Parcours la liste des apps fp
		$framapack='';$fp_statut='';
		if(isset($fp[$app['name']])) {
			$fp_statut = compareVersion($app['version'], $fp[$app['name']]['version']);
			$fp_admin = ($admin) ? '<a href="http://www.framapack.org/admin/edit_application.php?id='.$fp[$app['name']]['id'].'" title="Éditer"><img src="images/edit.png" alt="E" /></a>' : '';
			$framapack = '
				<input type="checkbox" id="app_'.$fp[$app['name']]['id'].'" value="'.$fp[$app['name']]['id'].'" />
				<span class="hs">_fp</span>
				<span class="tip">
				<span class="over">'.$fp_admin.'
					<a href="http://www.framapack.org/?share='.$fp[$app['name']]['id'].'" title="Télécharger"><img src="images/dl.png" alt="TP" /></a></span>
					<img src="images/cv'.$fp_statut.'.png" alt="'.$fp_statut.'" /><span class="statut">'.$fp_statut.'</span> '.$fp[$app['name']]['version'].'
				</span>
			';
		}

		// Parcours la liste des apps fk
		$framakey='';$fk_statut=''; $fk_a='';
		if(isset($fk[$app['name']])) {
			$fk_statut = compareVersion($app['version'], $fk[$app['name']]['version']);
			if ($fk_statut == 1 && $admin) {
				if(!$app['fk']) { // Ajoute le "lien" pour passage en Urgent
					$fk_a = ' class="fk" title="À mettre rapidement à jour"';
				} else { // Affiche le statut Urgent
					$fk_statut=0;
				}
			}
			$framakey = '
				<span class="hs">_fk</span>
				<span class="tip">
				<span class="over"><a href="'.$fk[$app['name']]['page'].'" title="Framakey"><img src="images/fk.png" width="20px" alt="K" /></a>
				<a href="'.$fk[$app['name']]['download'].'" title="Télécharger"><img src="images/dl.png" alt="TK" /></a></span>
				<img src="images/cv'.$fk_statut.'.png" alt="'.$fk_statut.'" '.$fk_a.' /><span class="statut">'.$fk_statut.'</span> '.$fk[$app['name']]['version'].'
				</span>
			';
		}

		// Notes depuis Ubuntu API
		$rate='<span class="hs">0</span>';
		if(isset($ub[$app['name']])) {
			$rate = '<span class="hs">'.$ub[$app['name']]['note'].'</span><span class="staroff"><span class="star" style="width:'.(floor($ub[$app['name']]['note']*48/5)).'px"></span><span class="votes">'.$ub[$app['name']]['votes'].'</span></span>';
		}

		$img = (file_exists('cache/img/'.$app['name'].'.png')) ? 'cache/img/'.$app['name'].'.png' : 'images/site.png';
		$fs = ($app['fs']!='') ? '<a href="http://framalibre.org/content/'.$app['name'].'"><img src="images/fs.png" width="20px" alt="F" /></a><span class="hs">_fl</span>': '<a href="http://www.google.com/cse?cx=017931506014566040656%3Aiqp-lrqbqlu&q='.$app['name'].'" title="sur Framalibre"><img src="images/search.png" alt="F" /></a>';
		$wp = ($app['wp']!='') ? '<a href="http://fr.wikipedia.org/wiki/'.$app['wp'].'"><img src="images/wp.png" alt="W" /></a><span class="hs">_wp</span>': '<a href="http://fr.wikipedia.org/wiki/Special:Recherche?search='.$app['name'].'" title="sur Wikipédia"><img src="images/search.png" alt="W" /></a>';

		//Description depuis Framasoft
		if(isset($fst[$app['name']])) {
			$fs = ($fst[$app['name']]['desc']!='') ? '<span class="description">'.$fs.'<span>&nbsp;</span></span>' : $fs;
		}


		if($admin) {
			setlocale (LC_TIME, 'fr_FR.utf8','fra');
			$col1 = '<span class="tip">
				<span class="over" style="margin-left:0;width:80px"><a href="javascript:void(0)" class="app_scan" title="Actualiser"><img src="images/refresh.png" alt="A" /></a>
				<a href="javascript:void(0)" class="app_edit" title="Éditer"><img src="images/edit.png" alt="E" /></a></span>
				<span class="hs">'.filemtime('cache/apps/'.$app['name'].'.txt').'</span>
				'.substr(ucfirst(strftime('%a', filemtime('cache/apps/'.$app['name'].'.txt'))),0,2).strftime(' %H:%M', filemtime('cache/apps/'.$app['name'].'.txt')).'</span>';
			$img_statut='<img src="images/cs'.$app['statut'].'.png" alt="'.$app['statut'].'" /><span class="statut">'.$app['statut'].'</span> ';

		} else {
			$col1 = ''; // Checkbox pour liste
			$img_statut='';
		}

	    $result.= '
	    <tr id="'.$app['name'].'">
			<td>'.$col1.'</td>
			<td>'.$fs.$wp.'</td>
			<td><a href="'.$app['site'].'" title="Site"><img src="'.$img.'" alt="S" /></a><a href="'.$app['site'].'" class="app_name" title="Site">'.$app['name'].'</a></td>
			<td>'.$rate.'</td>
			<td>'.$img_statut.$app['version'].'</td>
			<td>'.$app['version_date'].'</td>
			<td>'.$framadvd.'</td>
			<td>'.$framadvde.'</td>
			<td>'.$framakey.'</td>
			<td>'.$framapack.'</td>
		</tr>';
		if(isset($bang[1])){
		switch ($bang[1]) {
				case "g":
					header('Location: http://www.google.com/cse?cx=017931506014566040656%3Aiqp-lrqbqlu&q='.$bang[0]);
				break;
				case "b":
					if($app['name']==$bang[0]) {
						header('Location: http://'.$_SERVER['SERVER_NAME'].str_replace('index.php', '',$_SERVER['SCRIPT_NAME']).'#'.$app['name']);
					}
				break;
				case "k":
					if(isset($fk[$app['name']]['page']) && $app['name']==$bang[0]) {
						header('Location: '.$fk[$app['name']]['page']);
					}
					break;
				case "s":
					if(isset($app['site']) && $app['name']==$bang[0]) {
						header('Location: '.$app['site']);
					}
					break;
				case "f":
					if($app['name']==$bang[0]) {
						if($app['fs']!='') {
							header('Location: http://framalibre.org/'.$app['fs']);
						} else {
							header('Location: http://www.google.com/cse?cx=017931506014566040656%3Aiqp-lrqbqlu&q='.$app['name']);
						}
					}
					break;
				case "w":
					if($app['name']==$bang[0]) {
						if($app['wp']!='') {
							header('Location: http://fr.wikipedia.org/wiki/'.$app['wp']);
						} else {
							header('Location: http://fr.wikipedia.org/wiki/Special:Recherche?search='.$app['wp']);
						}
					}
					break;
				case "tk":
					if(isset($fk[$app['name']]['download']) && $app['name']==$bang[0]) {
						header('Location: '.$fk[$app['name']]['download']);
					}
					break;
				case "tp":
					if(isset($fp[$app['name']]['id']) && $app['name']==$bang[0]) {
						header('Location: http://www.framapack.org/?share='.$fp[$app['name']]['id']);
					}
					break;
			}
		}
	}
}

$atom = ($admin) ? '?atom=admin' : '';
//HTML pour l'affichage du tableau
echo '<!DOCTYPE HTML>
<html>
<head>
<title>Framabot</title>
    <meta charset="utf-8">
    <meta name="robots" content="nofollow" />
    <link rel="alternate" type="application/atom+xml" title="Atom 1.0" href="scan.php'.$atom.'" />
    <link rel="search" type="application/opensearchdescription+xml" href="search.php" title="Framabot Search" />
    <script src="https://framasoft.org/nav/lib/jquery/jquery.min.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/fontawesome.min.css">
</head>
<body>
    <script src="https://framasoft.org/nav/nav.js" type="text/javascript"></script>
	<div id="page">
	<div class="header">
	<link rel="stylesheet" type="text/css" href="css/tablesort.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script type="text/javascript" src="js/tablesort.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>



	<div id="cartbox">
		<a href="http://www.framapack.org/?share=" title="Télécharger avec Framapack" class="btn btn-info m-1">
		<i></i>Télécharger <br />
		<span id="cartcount">0</span> logiciel(s)</a>
	</div>
	<div id="colselect">
		<label class="checkbox-inline">
			<input type="checkbox" id="chk_fd"> FramaDVD
		</label>
		<label class="checkbox-inline">
			<input type="checkbox" id="chk_fe"> DVD École
		</label>
		<br />
		<label class="checkbox-inline">
			<input type="checkbox" id="chk_fk" checked="checked"> Framakey
		</label>
		<label class="checkbox-inline">
			<input type="checkbox" id="chk_fp" checked="checked"> Framapack
		</label>
	</div>
	<h1 class="title"><span style="color: rgb(106, 86, 135);">Frama</span><span style="color: rgb(12, 91, 122);">bot</span></h1>
';
// Infos
$legend = '
<div id="infos" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px; margin-left: -100px;" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Infos</h3>
			</div>
			<div class="modal-body" style="max-height:500px">';

if($admin) {
	//Légende pour admin
$legend .= '
		<table>
			<tr><th colspan="4">Légende</th></tr>
			<tr>
				<td><img src="images/cs2.png" alt="2" /></td>
				<td>Connexion ok - N° version détecté</td>
				<td><img src="images/cv2.png" alt="2" /></td>
				<td>Logiciel à jour</td>
			</tr>
			<tr>
				<td><img src="images/cs1.png" alt="1" /></td>
				<td>Connexion ok - N° version introuvable</td>
				<td><img src="images/cv1.png" alt="1" /></td>
				<td>Logiciel à mettre à jour</td>
			</tr>
			<tr>
				<td><img src="images/cs0.png" alt="0" /></td>
				<td>Connexion au site impossible</td>
				<td><img src="images/cv0.png" alt="0" /></td>
				<td>Logiciel à mettre à jour rapidement</td>
			</tr>
			<tr>
				<td><img src="images/cs00.png" alt="00" /></td>
				<td>Connexion au site impossible depuis longtemps</td>
				<td></td>
				<td></td>
			</tr>
		</table>';
} else {
	//Légende pour utilisateurs
$legend .= '
		<table>
			<tr><th colspan="4">Légende</th></tr>
			<tr>
				<td><img src="images/cv2.png" alt="2" /></td>
				<td>À jour</td>
				<td><img src="images/cv1.png" alt="1" /></td>
				<td>Pas à jour</td>
			</tr>
		</table>';
};
//Légence de &bangs
$legend .= '
		<table>
			<tr>
				<th colspan="2" width="350">Recherche</th>
				<th colspan="2" width="350">Liste des !bang</th>
			</tr><tr>
				<td colspan="2" width="350">
					Rechercher un logiciel fonctionne aussi depuis la barre d\'adresse de votre navigateur, exemple&nbsp;: <em><a href="?s=firefox">?s=firefox</a></em>
				</td>
				<td colspan="2" width="350">
					Principe inspiré de <a href="https://duckduckgo.com/bang.html">DuckDuckGo</a> ;
					fonctionne avec un lien ou dans la barre d\'adresse, exemple&nbsp;: <em><a href="?s=firefox!s">?s=firefox!s</a></em> redirige vers le site officiel de Firefox.
				</td>
			</tr><tr>
				<td><a href="?s=_fl">_fl</a></td>
				<td>Logiciels ayant un article sur Framalibre</td>
				<td><a href="?s=firefox!f">!f</a></td>
				<td><img width="20px" src="images/fs.png" alt=""/> - Article sur Framalibre</td>
			</tr><tr>
				<td><a href="?s=_wp">_wp</a></td>
				<td>Logiciels ayant un article sur Wikipédia</td>
				<td><a href="?s=firefox!w">!w</a></td>
				<td><img src="images/wp.png" alt=""/> - Article sur Wikipédia</td>
			</tr><tr>
				<td><a href="?s=_fd">_fd</a></td>
				<td>Logiciels disponibles sur le FramaDVD</td>
				<td><a href="?s=firefox!g">!g</a></td>
				<td><img src="images/search.png" alt=""/> - Recherche sur tout Framasoft</td>
			</tr><tr>
				<td><a href="?s=_fe">_fe</a></td>
				<td>Logiciels disponibles sur le FramaDVD École</td>
				<td><a href="?s=firefox!k">!k</a></td>
				<td><img width="20px" src="images/fk.png" alt=""/> - Page Framakey</td>
			</tr><tr>
				<td><a href="?s=_fk">_fk</a></td>
				<td>Logiciels disponibles sur la Framakey</td>
				<td><a href="?s=firefox!tk">!tk</a></td>
				<td><img src="images/dl.png" alt=""/> - Téléchargement du .zip Framakey</td>
			</tr><tr>
				<td><a href="?s=_fp">_fp</a></td>
				<td>Logiciels disponibles sur Framapack</td>
				<td><a href="?s=firefox!tp">!tp</a></td>
				<td><img src="images/dl.png" alt=""/> - Framapack.exe du logiciel</td>
			</tr>
		</table>
			</div>
		</div>
	</div>
</div>
';


if($admin){echo'
	<script type="text/javascript" src="js/admin.js"></script>

';
} else {
	//Login form
	echo '
	<div id="login_form" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3>Se connecter</h3>
				</div>
				<form action="index.php" method="post" role="form" class="form-horizontal">
				<div class="modal-body">
					<div class="form-group">
						<label for="user_id">Identifiant</label>
						<input type="text" class="form-control" name="user_id" placeholder="Identifiant">
					</div>
					<div class="form-group">
						<label for="user_pwd">Mot de passe</label>
						<input type="password" class="form-control" name="user_pwd" placeholder="Mot de passe">
					</div>
				</div>
				<div class="modal-
				">
					<button type="submit" class="btn btn-success">Connexion</button>
				</div>
				</form>
			</div>
		</div>
	</div>';
}
echo $legend.'
	<div id="search" class="hs">'.$bang[0].'</div>
	</div>
	<div class="body">
		<table id="bot_results" class="table-sort table-sort-search table-sort-show-search-count">
            <thead>
                <tr>
                    <th class="table-sort"></th>
                    <th></th>
                    <th class="table-sort">Logiciel zigoto</th>
                    <th class="table-sort">Note</th>
                    <th class="table-sort">Version</th>
                    <th class="table-sort">Date</th>
                    <th class="table-sort">FramaDVD</th>
                    <th class="table-sort">DVD École</th>
                    <th class="table-sort">Framakey</th>
                    <th class="table-sort">Framapack</th>
                </tr>
            </thead>
            <tbody>
				'.$result.'
            </tbody>
		</table>
	</div>
	<div class="footer"></div>
	</div>
</body>
</html>';
?>

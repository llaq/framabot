<?php
$check = isset($_GET["c"]) ? $_GET["c"] : false;
$force = isset($_GET["f"]) ? true : false; // Paramètre pour forcer l'écriture dans le cache
$atom = isset($_GET["atom"]) ? $_GET["atom"] : false; // Paramètre pour forcer l'écriture dans le cache


$log = fopen('log/'.date("Ymd", time()).'.txt', 'a+');
$log_txt ='';

require_once('config.php');
session_start();
if(isset($_SESSION['login']) && isset($_SESSION['pass']) && md5($_SESSION['login'].':'.$_SESSION['pass'])!=$login_pass) { // SECURITE : Seul un admin peut contourner le minuteur
	$force=false;
}

function alias($name) {
	global $rename;
	$rep_name=strtolower($name);
	foreach($rename as $old_name => $new_name) {
		if (strtolower($name)==$old_name) {
			$rep_name = strtolower($new_name);
		}
	}
	return $rep_name;
}


function scan_ubuntu() {
	$ub_file = @file_get_contents('https://reviews.ubuntu.com/reviews/api/1.0/review-stats/ubuntu/any/'); // Scanne ubuntu
	// $ub_comment = https://reviews.ubuntu.com/reviews/api/1.0/reviews/filter/fr/ubuntu/any/any/firefox/
	if($ub_file !== false){
		$ub_rate = json_decode($ub_file, true);
		// Sort un tableau propre 'app' => 'note'
		$ub_apps = array();
		for($i=0; $i<count($ub_rate); $i++) { 
			$app_name = alias($ub_rate[$i]['package_name']); // Renomme s'il y a un alias (et met tout en minuscule)
			$ub_apps[$app_name] = array (
				'note' => $ub_rate[$i]['ratings_average'],
				'votes' => $ub_rate[$i]['ratings_total'],
			);
		}
		// Met les données en cache : ubuntu.txt
		$ub_txt = fopen('cache/ubuntu.txt', 'wb');
		fwrite($ub_txt, json_encode($ub_apps));	fclose($ub_txt);
	}
}

/*************************	
*        FRAMAPACK
* ************************/
function scan_framapack() {
	global $log; $log_txt='';
	$framapack = @file_get_contents('http://framapack.org'); // Scanne framapack.org
	if($framapack !== false){
		preg_match_all('#<img src="logo/(.*?).png"#', $framapack, $apps); // Récupère le nom de chaque logiciel
		preg_match_all('#</b> <small>(.*?)</small><br />#', $framapack, $versions); // Récupère la version
		preg_match_all('# for="application_([0-9]*?)">#', $framapack, $ids); // Récupère l'id du logiciel sur FP
		// Sort un tableau propre 'app' => 'version'
		$fp_apps = array(); 
		$file = file('cache/framapack.txt');
		$fp_old_apps = json_decode($file[0], true);
		for($i=0; $i<count($apps[1]); $i++) { 
			$app_name=alias($apps[1][$i]);// Renomme s'il y a un alias (et met tout en minuscule)
			$fp_apps[$app_name] = array (
				'version' => $versions[1][$i],
				'id' => $ids[1][$i]
			);
			if(isset($fp_old_apps[$app_name])) {
				if($fp_apps[$app_name]['version']!=$fp_old_apps[$app_name]['version']) {
					$log_txt .= '[Framapack]['.$app_name.'] Mise à jour en version : '.$fp_apps[$app_name]['version'].' (version précédente : '.$fp_old_apps[$app_name]['version'].').
';				}
			} else {
				$log_txt .= '[Framapack]['.$app_name.']('.$fp_apps[$app_name]['version'].') Le logiciel a été ajouté.
';			}			
		}
		// Met les données en cache : framapack.txt
		$fp_txt = fopen('cache/framapack.txt', 'wb');
		fwrite($fp_txt, json_encode($fp_apps));	fclose($fp_txt);
	}
	fwrite($log, $log_txt);
}

/*************************	
*        FRAMAKEY
* ************************/
function scan_framakey() {
	global $log; $log_txt='';
	$sfk = @file_get_contents('http://files.framakey.org/stable/main.xml'); // Scanne la liste des logiciels du dépôt stable
	$tfk = @file_get_contents('http://files.framakey.org/testing/main.xml'); // Idem testing
	if($sfk !== false && $tfk!== false){
		$fk = $sfk.$tfk;
		preg_match_all('# parentname="(.*?)"#', $fk, $apps); // Récupère le nom de chaque logiciel dans stable
		preg_match_all('#" version="(.*?)">#', $fk, $versions); // Récupère la version
		preg_match_all('#">http://files.framakey.org/(.*?).fmk.zip</download>#', $fk, $download);
		preg_match_all('#<framakey>(.*?)</framakey>#', $fk, $page);		
		// Sort un tableau propre 'app' => 'version'
		$fk_apps = array(); 
		$file = file('cache/framakey.txt');
		$fk_old_apps = json_decode($file[0], true);
		for($i=0; $i<count($apps[1]); $i++) {
			$app_name = alias($apps[1][$i]); // Renomme s'il y a un alias (et met tout en minuscule)
			$version = (substr($versions[1][$i], -2, 2)==".0") ? substr($versions[1][$i], 0,-2) : $versions[1][$i];
			$version = (substr($version, -2, 2)==".0") ? substr($version, 0,-2) : $version; 
			if(isset($fk_apps[$app_name]['version']) && compareVersion($fk_apps[$app_name]['version'],$version)==1) {
				// Vérification de version pour les cas où on maintient plusieurs branches
				// Si la nouvelle version est antérieure on ne modifie pas $fk_apps[$app_name]
				// ex: Spip 2.1.19 et Spip 3.0.5 => Spip 3.0.5 l'emporte
			} else {
				$fk_apps[$app_name] = array(
					'version' => $version,
					'page' => 'http://'.str_replace('http://','',$page[1][$i]),
					'download' => 'http://files.framakey.org/'.$download[1][$i].'.fmk.zip',
				);
			}
			
			if(isset($fk_old_apps[$app_name]) ) {
				if(compareVersion($fk_apps[$app_name]['version'],$fk_old_apps[$app_name]['version'])==1) {
					$log_txt .= '[Framakey]['.$app_name.'] Mise à jour en version : '.$fk_apps[$app_name]['version'].' (version précédente : '.$fk_old_apps[$app_name]['version'].').
';				}
			} else {
				$log_txt .= '[Framakey]['.$app_name.']('.$fk_apps[$app_name]['version'].') Le logiciel a été ajouté.
';			}	
		}			
		// Sauvegarde les données en cache : framakey.txt
		$fk_txt = fopen('cache/framakey.txt', 'wb');
		fwrite($fk_txt, json_encode($fk_apps));	fclose($fk_txt);
	}
	fwrite($log, $log_txt);
}


/*************************	
*        APPS
* ************************/
function scan_app($name) {
	global $log; $log_txt='';
	$file = @file('cache/apps/'.$name.'.txt');
	$log_txt=''; $tab='';
	if ($file != false){// Le logiciel existe dans le cache
		$tab = json_decode($file[0], true);
		if($tab['urltoscan']!='' && $tab['pattern']!='') {		
		/*******************************
		 *  Dans tab il y a 
		 * 		name 		([a-z]+-)
		 * 		site		(url)
		 * 		fs			(article0000.html)
		 * 		wp			("Firefox")
		 * 
		 * 		urltoscan	(url)
		 * 		pattern		(#texte([0-9\.]+)texte#)
		 * 
		 * 		statut		(0-2)
		 * 		offline 	(0-n)
		 * 		version		(2.1)
		 * 		version_date(2013-06-27)
		 * 
		 * 		fk			(false)
		 * 
		 ********************************/			
			$content = @file_get_contents($tab['urltoscan']);
			if($content === false){ 
				//On vérifie la connexion
				if($tab['offline']<30) {
					$tab['statut']= 0;
				} else {  // orage à partir de 30 passages
					$log_txt .= '['.$name.'][Erreur] Connexion au site impossible depuis longtemps.
';					$tab['statut']= '00';
				};
				$tab['offline']++;
			} elseif(preg_match($tab['pattern'], $content, $matches) === 0) {
				//On cherche le n° de version
				$log_txt .= '['.$name.'][Erreur] Le motif de l\'expression régulière est incorrect.
';				$tab['statut']=1;$tab['offline']=0;
			} else {
				$tab['statut']=2;$tab['offline']=0;
				if ($tab['version']!=$matches[1]) {
					// On en trouve un nouveau
					$log_txt .= '['.$name.'] Nouvelle version : '.$matches[1].' (version précédente : '.$tab['version'].').
';			
					// Lecture des fichiers Framapack et Framakey
					$file = file('cache/framapack.txt');$fp = json_decode($file[0], true);
					$file = file('cache/framakey.txt');$fk = json_decode($file[0], true);
					if(isset($fp[$name])) {
						$log_txt .= '[Framapack]['.$name.'] À mettre à jour : '.$matches[1].'
';					}
					if(isset($fk[$name])) {
						$log_txt .= '[Framakey]['.$name.'] À mettre à jour : '.$matches[1].'
';					}			
					$tab['version']=$matches[1];
					$tab['version_date']=date('Y-m-d');
				}
			}
			// Sauvegarde les données en cache
			$app_txt = fopen('cache/apps/'.$name.'.txt', 'wb');
			fwrite($app_txt, json_encode($tab));fclose($app_txt);
		} else {
			$log_txt .= '['.$name.'][Erreur] Le logiciel n\'est pas configuré pour être scanné.
';		}
	} else {
		$log_txt .= '['.$name.'][Erreur] Le logiciel n\'existe pas dans le cache.
';	}
	fwrite($log, $log_txt);
	return $tab;
}


if($force===true) {
	switch ($check) {
		case "ubuntu": scan_ubuntu(); break;
		case "framapack": scan_framapack(); break;
		case "framakey": scan_framakey(); break;
		default: 
			if ($check!=false) { scan_app($check); };
		break;
	}
} else {
	if (filemtime('cache/ubuntu.txt')+$delay<time()) {scan_ubuntu();}
	if (filemtime('cache/framapack.txt')+$delay<time()) {scan_framapack();}
	if (filemtime('cache/framakey.txt')+$delay<time()) {scan_framakey();}
	/*************************	
	*        SCAN PLANIFIE
	* ************************/
	// Parcours du cache apps
	$file = file('cache/framabot.txt');
	$fb = json_decode($file[0], true);	
	$start = $fb[0]['_meta_pointer_']; // Pointeur du dernier logiciel scanné
	$end = ($start+10 < count($fb)-1) ? $start+10 : count($fb)-1;
	sort($fb);
	for($i=$start; $i<=$end; $i++) {
		$meta_date = @filemtime('cache/apps/'.$fb[$i]['name'].'.txt');
		if($meta_date!==false && $meta_date+$delay<time()) {
			// Effectue le scan et copie la portion de tableau dans framabot.txt
			$fb[$i]=scan_app($fb[$i]['name']);
		}
	}
	// Mise à jour du pointeur pour prochain passage
	$fb[0]['_meta_pointer_'] = ($end+1<count($fb)-1) ? $end+1 : 1;		
	// Met les données en cache : framabot.txt
	$fb_txt = fopen('cache/framabot.txt', 'wb');
	fwrite($fb_txt, json_encode($fb)); fclose($fb_txt);
}

fclose($log);
echo '<feed xml:lang="fr-fr" xmlns="http://www.w3.org/2005/Atom">
     <title>Framabot</title>
     <subtitle></subtitle>
     <link href="scan.php?atom='.$atom.'" rel="self"/>
     <updated>'.date(DATE_ATOM).'</updated>
     <author>
          <name>Framasoft</name>
          <email>tech@framasoft.org</email>
     </author>
     <id>Framabot</id>';
$log_open = @opendir('log');
$entries=array(); $i=0;
while ($file=readdir($log_open)) {
	if (substr($file,0,1)!=".") {
		$log_date=mktime('0','0','0',substr($file,4,2)+0,substr($file,6,2)+0,substr($file,0,4)+0);
		if ((time()-7*24*60*60)<$log_date && $log_date<(time()-24*60*60)) {
			$log_file=file('log/'.$file);
			$clean_log=array_unique($log_file);
			sort($clean_log);
			$summary='';
			switch ($atom) {
				case 'admin' :
					foreach($clean_log as $line) {$summary .= $line.'&lt;br /&gt;';};
					break;
				case 'framakey' :
					foreach($clean_log as $line) {$summary .= (strstr($line,'Framakey') && !strstr($line,'Erreur') && !strstr($line,'mettre à jour')) ? $line.'&lt;br /&gt;' : '';};
					break;
				case 'framapack' :
					foreach($clean_log as $line) {$summary .= (strstr($line,'Framapack') && !strstr($line,'Erreur') && !strstr($line,'mettre à jour')) ? $line.'&lt;br /&gt;' : '';};
					break;
				default :
					foreach($clean_log as $line) {$summary .=  (!strstr($line,'Framabot') && !strstr($line,'Erreur')) ? $line.'&lt;br /&gt;' : '';};
					break;
			};
			$entries[$i]= array(
				'updated' => $log_date,
				'summary' => $summary
			);
		}
	}
	$i++;
}
rsort($entries);
foreach($entries as $key) {
	echo '
		<entry>
			<title>Log du '.date("d-m-Y", $key['updated']).'</title>
			<link type="text/html" href="/"/>
			<id>'.date(DATE_ATOM, $key['updated']).'</id>
			<updated>'.date(DATE_ATOM, $key['updated']).'</updated>
			<author>
				<name>Framabot</name>
			</author>
			<summary type="html">'.$key['summary'].'</summary>
		</entry>
';
}
echo '
</feed>';
?>
